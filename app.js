//Parse Variables
var Parse = require('parse/node');
//var moment = require('moment-timezone');
var APPLICATION_ID = "myAppId";
var JS_KEY = "myJavascriptKey";
var M_KEY = "masterKey";
//var SERVER_URL = "http://localhost:8081/parse";
//var REPORT_URL = "http://localhost:8081/summary?";
var SERVER_URL = "http://meshcandy-staging.us-east-1.elasticbeanstalk.com/parse";
var REPORT_URL = "http://meshcandy-staging.us-east-1.elasticbeanstalk.com/summary?";
Parse.masterKey = M_KEY;
Parse.serverURL = SERVER_URL;
Parse.initialize(APPLICATION_ID, JS_KEY, M_KEY);

var role;

init()
.then(() => main());

function init() {
	return new Promise((resolve, reject) => {
        prompt("Choose:\n(1) Login\n(2) Signup\n(x) Exit")
        .then((choice) => {
            if(choice == 1) {
                resolve(login());
            } else if (choice == 2) {
        
            } else if (choice == 'x') {
                process.exit();
            } else {
                console.log("Invalid Input");
                resolve(init());
            }
        })   
    }); 
}
function addDevices () {
	return new Promise((resolve, reject) => {
        prompt("What do you want to create?:\n(1) Mobile Station\n(2) Beacon Temperature Probe\n(3) Beacon Tracker\n(4) Beacon Observer\n(5) Kit\n(x) Exit")
        .then((choice) => {
            if(choice == 1) {
                resolve(addMobileStation());
            } else if (choice == 2) {
                resolve(addBeaconTempProbe());
            } else if (choice == 3) {
                resolve(addBeaconTracker());
            } else if (choice == 4) {
                resolve(addBeaconObserver());
            } else if (choice == 5) {
                resolve(addKit());
            } else if (choice == 'x') {
                process.exit();
            } else {
                console.log("Invalid Input");
                resolve(addDevices());
            }
        })   
    });
}

function addMobileStation (auto) {
	return new Promise((resolve, reject) => {
        var tag_register = Parse.Object.extend("tag_register");
        var tag = new tag_register();
		var acl = new Parse.ACL();
		acl.setRoleWriteAccess(role.get("name"), true);
        acl.setRoleReadAccess(role.get("name"), true);
        tag.setACL(acl);
        tag.set("subsc_temp", true);
        tag.set("show", true);
        tag.set("domain", role);
        tag.set("control_id","");
        
        if(auto) {
            tag.set("common", auto.cname);
            prompt("Enter Serial Number for " + auto.cname)
            .then(sn => {
                tag.set("sn", sn);
                return tag.save();
            }).then(res => {
                console.log(res);
                resolve();
            });
            
        } else {
            prompt("Enter Common Name")
            .then(cname => {
                tag.set("common", cname);
                return prompt("Enter Serial Number");
            }).then(sn => {
                tag.set("sn", sn);
                return tag.save();
            }).then(tagObj => {
                console.log(tagObj);
                return prompt("Add another?\n(1) Yes\n(2) No");
            }).then(choice => {
                if(choice == 1) {
                    resolve(addMobileStation());
                }else {
                    resolve(main());
                }
            }).catch(error => {
                console.error(error);
                resolve(main());
            });
        }
    });
}

function addBeaconTracker (auto) {
	return new Promise((resolve, reject) => {
        var tag_register = Parse.Object.extend("tag_register");
        var tag = new tag_register();
		var acl = new Parse.ACL();
		acl.setRoleWriteAccess(role.get("name"), true);
        acl.setRoleReadAccess(role.get("name"), true);
        tag.setACL(acl);
        tag.set("subsc_rssi", true);
        tag.set("show", true);
        tag.set("domain", role);
        tag.set("isBeacon", true);

        var BeaconUUID = Parse.Object.extend("BeaconUUID");
        var buuid = BeaconUUID.createWithoutData("6t4esio1i4");
        tag.set("bUuid", buuid);
        tag.set("control_id","");

        if(auto) {
            tag.set("common", auto.cname);
            prompt("Enter Major Beacon ID")
            .then(major => {
                tag.set("major", major);
                return prompt("Enter Minor Beacon ID");
            }).then(minor => {
                tag.set("minor", minor);
                return tag.save();
            }).then(res => {
                console.log(res);
                resolve();
            });
            
        } else {
            prompt("Enter Common Name")
            .then(cname => {
                tag.set("common", cname);
                return prompt("Enter Major Beacon ID for " + auto.cname);
            }).then(major => {
                tag.set("major", major);
                return prompt("Enter Minor Beacon ID for " + auto.cname);
            }).then(minor => {
                tag.set("minor", minor);
                return tag.save();
            }).then(tagObj => {
                console.log(tagObj);
                return prompt("Add another?\n(1) Yes\n(2) No");
            }).then(choice => {
                if(choice == 1) {
                    resolve(addBeaconTracker());
                }else {
                    resolve(main());
                }
            }).catch(error => {
                console.error(error);
                resolve(main());
            });
        }
    });
}

function addBeaconTempProbe (auto) {
	return new Promise((resolve, reject) => {
        var tag_register = Parse.Object.extend("tag_register");
        var tag = new tag_register();
		var acl = new Parse.ACL();
		acl.setRoleWriteAccess(role.get("name"), true);
        acl.setRoleReadAccess(role.get("name"), true);
        tag.setACL(acl);
        tag.set("subsc_temp", true);
        tag.set("show", true);
        tag.set("domain", role);
        tag.set("isBeacon", true);
        tag.set("control_id","");

        var BeaconUUID = Parse.Object.extend("BeaconUUID");
        var buuid = BeaconUUID.createWithoutData("TJHJ70JL5H");
        tag.set("bUuid", buuid);

        if(auto) {
            tag.set("common", auto.cname);
            prompt("Enter Major Beacon ID for " + auto.cname)
            .then(major => {
                tag.set("major", major);
                return prompt("Enter Minor Beacon ID for " + auto.cname);
            }).then(minor => {
                tag.set("minor", minor);
                return tag.save();
            }).then(res => {
                console.log(res);
                resolve();
            });
            
        } else {
            prompt("Enter Common Name")
            .then(cname => {
                tag.set("common", cname);
                return prompt("Enter Major Beacon ID");
            }).then(major => {
                tag.set("major", major);
                return prompt("Enter Minor Beacon ID");
            }).then(minor => {
                tag.set("minor", minor);
                return tag.save();
            }).then(tagObj => {
                console.log(tagObj);
                return prompt("Add another?\n(1) Yes\n(2) No");
            }).then(choice => {
                if(choice == 1) {
                    resolve(addBeaconTempProbe());
                }else {
                    resolve(main());
                }
            }).catch(error => {
                console.error(error);
                resolve(main());
            });
        }
    });
}

function addBeaconObserver (auto) {
	return new Promise((resolve, reject) => {
        var pd_register = Parse.Object.extend("pd_register");
        var pd = new pd_register();
		var acl = new Parse.ACL();
		acl.setRoleWriteAccess(role.get("name"), true);
        acl.setRoleReadAccess(role.get("name"), true);
        pd.setACL(acl);
        pd.set("domain", role);
        pd.set("control_id","");
        
        if(auto) {
            pd.set("common", auto.cname);
            prompt("Enter Serial Number for " + auto.cname)
            .then(sn => {
                pd.set("sn", sn);
                return pd.save();
            }).then(res => {
                console.log(res);
                resolve();
            });
            
        } else {
            prompt("Enter Common Name")
            .then(cname => {
                pd.set("common", cname);
                return prompt("Enter Serial Number");
            }).then(sn => {
                pd.set("sn", sn);
                return pd.save();
            }).then(pdObj => {
                console.log(pdObj);
                return prompt("Add another?\n(1) Yes\n(2) No");
            }).then(choice => {
                if(choice == 1) {
                    resolve(addBeaconObserver());
                }else {
                    resolve(main());
                }
            }).catch(error => {
                console.error(error);
                resolve(main());
            });
        }
    });
}

function addKit () {
	return new Promise((resolve, reject) => {
        addMobileStation({"cname": "Mobile Station 1"})
        .then(() => addMobileStation({"cname": "Mobile Station 2"}))
        .then(() => addBeaconTempProbe({"cname": "Temp Probe 1"}))
        .then(() => addBeaconTempProbe({"cname": "Temp Probe 2"}))
        .then(() => addBeaconObserver({"cname": "Beacon Observer 1"}))
        .then(() => addBeaconObserver({"cname": "Beacon Observer 2"}))
        .then(() => resolve(main()));
    });
}

function main () {
	return new Promise((resolve, reject) => {
        prompt("Choose:\n(1) Add Devices\n(x) Exit")
        .then((choice) => {
            if(choice == 1) {
                resolve(addDevices());
            } else if (choice == 'x') {
                process.exit();
            } else {
                console.log("Invalid Input");
                resolve(main());
            }
        })   
    }); 
}


function login() {
	return new Promise((resolve, reject) => {
        var username;
        var password;
        prompt("Enter Username")
        .then(uname => {
            username = uname;
            return promptSensitive("Enter Password")
        }).then(pword => {
            password = pword;
            console.log("Logging in as " + username);
            return;
        }).then(() => {
            Parse.User.logIn(username, password)
            .then(user => {
                console.log("Logged In Successfully!");
                console.log("Hello " + user.get("firstname") + "!");
                var query = new Parse.Query(Parse.Role);
                query.equalTo("users", user);
                query.include("domain");
                return query.first({useMasterKey: true});
            }).then(roleObj => {
                if(roleObj == undefined) reject("Account error");
                role = roleObj;
                console.log(role);
                resolve(1);
            }).catch(error => {
                console.error(error.message);
                resolve(login());
            });
        });
    });
}

function prompt(dialog) {
	return new Promise((resolve, reject) => {
		console.log(dialog);
		var stdin = process.stdin,
			stdout = process.stdout;
		stdin.resume();
		stdout.write("> ");
		stdin.once('data', function(data) {
			resolve(data.toString().trim());
			stdin.pause();
		});
	});
}

function promptSensitive(dialog) {
	return new Promise((resolve, reject) => {
        
        console.log(dialog);
        
        var readline = require('readline');
        var Writable = require('stream').Writable;

        var mutableStdout = new Writable({
        write: function(chunk, encoding, callback) {
            if (!this.muted)
            process.stdout.write(chunk, encoding);
            callback();
        }
        });

        mutableStdout.muted = false;

        var rl = readline.createInterface({
        input: process.stdin,
        output: mutableStdout,
        terminal: true
        });

        rl.question('Password: ', function(password) {
        rl.close();
        resolve(password);
        });

        mutableStdout.muted = true;
	});
}